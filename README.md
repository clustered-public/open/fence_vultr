I'll preface this by noting that it is a little bit of a work-in-progress, but ultimately working.

This is a custom fence (or stonith) device for Pacemaker which uses the Vultr API to allow your VC2 instances to be fenced.

## What does it do?

When implemented into Pacemaker, this allows the cluster manager to reboot your nodes (VMs) when it detects issues, or needs to do so in order to recover from a failure.

# Installation

**1.** Download the `fence_vultr` file from this repository, and move it into `/usr/sbin/`:

```
# mv /root/fence_vultr /usr/sbin/fence_vultr
```

**2.** Make the new file executable so that PCS can run it:

```
# chmod +x /usr/sbin/fence_vultr
```

**3.** Generate an API key for your Vultr account in [https://my.vultr.com/settings/#settingsapi](https://my.vultr.com/settings/#settingsapi).

I would recommend creating a sub-account in *Vultr > Account > Users*, with the **"Manage Servers"** permission only, and then logging into that to generate the API key for this script. Least privilege and all that...

**4.** Add the public IP address of the servers in the cluster that you want to be able to fence one another to the Vultr API section, otherwise it will reject the API calls as unauthorised.

**5.** Test your connection to Vultr from the script by requesting a list of VMs:

```
# /usr/sbin/fence_vultr --password=YOUR-API-KEY --action=list --shell-timeout=300
1234,server1.domain.com
1235,server2.domain.com
1236,server2.domain.com
```

If this step fails, you probably don't have the right API key or permissions on the API key. One day I'll add error handling for this.

The list that is returned is in the format of `subid,hostname` - the SUBID value is what will be used to determine which VM is fenced.

**6.** Test a reboot of one of the VMs, replacing `SUBID` on the `--plug` switch with the correct VM ID:

```
# /usr/sbin/fence_vultr --password=YOUR-API-KEY --action=reboot --plug=SUBID --shell-timeout=300
Success: Rebooted
```

**7.** Add it into PCS as a stonith device:

```
# pcs stonith create cluster-stonith fence_vultr passwd=YOUR-API-KEY action=reboot power_wait=120 delay=5 pcmk_host_map="server1:1234;server2:1235"
```

The `pcmk_host_map` directive here is used to map the hostname according to PCS to the ID according to Vultr in the format of `hostname:subid`. When one of the nodes wants to fence, it sends PCS the hostname of the problem node, and PCS converts that to the SUBID ready for the Vultr API.

Good luck :)